export const LOGO = require('../src/assets/images/logo.png')
export const LOGO_WIDTH = require('../src/assets/images/logo_width.png')
export const LOGO_VERTICAL = require('../src/assets/images/logo_vertical.png')
export const SPLASH_IMAGE = require('../src/assets/images/splash_image.png')
export const AVTAR = require('../src/assets/images/avatar.jpg')
export const CAMERA = require('../src/assets/images/camera.png')
export const CLOSE_RED = require('../src/assets/images/close.png')
export const EVENT_MARKER = require('../src/assets/images/event_marker.png')
export const FACEBOOK = require('../src/assets/images/facebook.png')
export const LOCATION_GREEN = require('../src/assets/images/location_gree_org.png')
export const RIGHT_GREEN = require('../src/assets/images/right_green.png')
export const RIGHT_RED = require('../src/assets/images/right_red.png')
export const SHARED_LOC = require('../src/assets/images/share_loc.png')
export const IC_ALL_INDIVIDUALS = require('../src/assets/images/ic_all_individuals.png')
export const IC_POPULAR_INDIVIDUALS = require('../src/assets/images/ic_popular_individuals.png')
export const IC_POPULAR_ORG = require('../src/assets/images/ic_popular_organisation.png')
export const IC_ALL_ORG = require('../src/assets/images/ic_all_organisations.png')
export const IC_ALL_ = require('../src/assets/images/ic_all_organisations.png')
export const IC_POPULAR_ = require('../src/assets/images/ic_popular_organisation.png')
export const IC_POPULAR_INDIVIDUALS_ = require('../src/assets/images/ic_popular_individuals.png')
export const IC_ALL_INDIVIDUALS_ = require('../src/assets/images/ic_all_individuals.png')
export const IC_GAL_ = require('../src/assets/images/gal.jpg')



export default {
  LOGO,
  LOGO_WIDTH,
  LOGO_VERTICAL,
  SPLASH_IMAGE,
  AVTAR,
  CAMERA,
  CLOSE_RED,
  EVENT_MARKER,
  FACEBOOK,
  LOCATION_GREEN,
  RIGHT_GREEN,
  RIGHT_RED,
  SHARED_LOC,
  IC_ALL_INDIVIDUALS,
  IC_POPULAR_INDIVIDUALS,
  IC_POPULAR_ORG,
  IC_ALL_ORG,
  IC_ALL_,
  IC_POPULAR_,
  IC_POPULAR_INDIVIDUALS_,
  IC_ALL_INDIVIDUALS_,
  IC_GAL_
}