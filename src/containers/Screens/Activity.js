import { connect } from 'react-redux'

import Activity from '../../components/Screens/Activity'
import {
  getActivityApi,
} from '../../thunks'

const mapStateToProps = ({ Activity  }) => ({
    loading: Activity.loading,
    error:Activity.error,
    data:Activity.data
})

const mapDispatchToProps = {
  requestGetActivity: getActivityApi,
}

export default connect(mapStateToProps, mapDispatchToProps)(Activity)