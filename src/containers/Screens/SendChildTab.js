import { connect } from 'react-redux'

import SendChildTab from '../../components/Screens/SendChildTab'
import {
    getSearchCustomerListApi,
} from '../../thunks'

const mapStateToProps = ({ SendChildTab  }) => ({
    loading: SendChildTab.loading,
    error:SendChildTab.error,
    data:SendChildTab.data
})

const mapDispatchToProps = {
    requestGetSearchCustomerList: getSearchCustomerListApi,   
}

export default connect(mapStateToProps, mapDispatchToProps)(SendChildTab)