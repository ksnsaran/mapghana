import { connect } from 'react-redux'

import SendRequestExchange from '../../components/Screens/SendRequestExchange'
import {
    fiatToFiatConversionApi,
    feesSendTransactionApi,
    sendReceiveRequestApi,
    twoFactorVerifySendRequestApi
} from '../../thunks'

const mapStateToProps = ({ SendRequestExchange  }) => ({
    loading: SendRequestExchange.loading,
    error:SendRequestExchange.error,
    data:SendRequestExchange.data
})

const mapDispatchToProps = {
    requestFiatToFiatConversion: fiatToFiatConversionApi,   
    requestFeesSendTransaction: feesSendTransactionApi,   
    requestSendReceiveRequest: sendReceiveRequestApi,   
    requestTwoFactorVerifySendRequest: twoFactorVerifySendRequestApi,   
}

export default connect(mapStateToProps, mapDispatchToProps)(SendRequestExchange)