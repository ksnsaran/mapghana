import { connect } from 'react-redux'

import ContactChildTab from '../../components/Screens/ContactChildTab'
import {
    getContactsApi
} from '../../thunks'

const mapStateToProps = ({ ContactChildTab  }) => ({
    loading: ContactChildTab.loading,
    error:ContactChildTab.error,
    data:ContactChildTab.data
})

const mapDispatchToProps = {
    requestGetContactsList: getContactsApi,   
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactChildTab)