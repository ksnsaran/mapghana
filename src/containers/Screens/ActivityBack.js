import { connect } from 'react-redux'

import ActivityBack from '../../components/Screens/ActivityBack'
import {
    getActivityBackApi,
} from '../../thunks'

const mapStateToProps = ({ ActivityBack  }) => ({
    loading: ActivityBack.loading,
    error:ActivityBack.error,
    data:ActivityBack.data
})

const mapDispatchToProps = {
  requestGetActivityBack: getActivityBackApi,
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityBack)