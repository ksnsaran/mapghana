import { connect } from 'react-redux'

import ContactInfo from '../../components/Screens/ContactInfo'
import {
    getCustomerActivityApi
} from '../../thunks'

const mapStateToProps = ({ ContactInfo  }) => ({
    loading: ContactInfo.loading,
    error:ContactInfo.error,
    data:ContactInfo.data
})

const mapDispatchToProps = {
    requestGetCustomerActivity: getCustomerActivityApi,   
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactInfo)