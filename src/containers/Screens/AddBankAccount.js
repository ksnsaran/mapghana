
import { connect } from 'react-redux'

import AddBankAccount from '../../components/Screens/AddBankAccount'
import {
    getBankDetailApi,
    addBankDetailApi,
    updateBankDetailApi
} from '../../thunks'

const mapStateToProps = ({ AddBankAccount  }) => ({
    loading: AddBankAccount.loading,
    error:AddBankAccount.error,
    data:AddBankAccount.data
})

const mapDispatchToProps = {
    requestGetBankDetail: getBankDetailApi,
    requestAddBankDetail: addBankDetailApi,
    requestUpdateBankDetail: updateBankDetailApi,
}

export default connect(mapStateToProps, mapDispatchToProps)(AddBankAccount)