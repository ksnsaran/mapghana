import { connect } from 'react-redux'

import Profile from '../../components/Screens/Profile'
import {
  getProfileApi,
  getPaymentQRCodeApi,
  updateProfilePictureApi
} from '../../thunks'

const mapStateToProps = ({ Profile  }) => ({
    loading: Profile.loading,
    error:Profile.error,
    data:Profile.data
})

const mapDispatchToProps = {
  requestGetProfile: getProfileApi,
  requestGetPaymentQRCode: getPaymentQRCodeApi,
  requestUpdateProfilePicture: updateProfilePictureApi,
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)